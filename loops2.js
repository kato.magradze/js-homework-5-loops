const tourists = [];

tourists[0] = {
  name: "Mark",
  age: 19,
  cities: ["Tbilisi", "London", "Rome", "Berlin"],
  money: [120, 200, 150, 140],
};

tourists[1] = {
  name: "Bob",
  age: 21,
  cities: ["Miami", "Moscow", "Vienna", "Riga", "Kiev"],
  money: [90, 240, 100, 76, 123],
};

tourists[2] = {
  name: "Sam",
  age: 22,
  cities: ["Tbilisi", "Budapest", "Warsaw", "Vilnius"],
  money: [118, 95, 210, 236],
};

tourists[3] = {
  name: "Anna",
  age: 20,
  cities: ["New York", "Athens", "Sydney", "Tokyo"],
  money: [100, 240, 50, 190],
};

tourists[4] = {
  name: "Alex",
  age: 23,
  cities: ["Paris", "Tbilisi", "Madrid", "Marseille", "Minsk"],
  money: [96, 134, 76, 210, 158],
};

// CHECK IF 21+

for (let i = 0; i < tourists.length; i++) {
  tourists[i].adult = tourists[i].age >= 21;
}

// CHECK IF THE TOURIST HAS BEEN TO TBILISI

for (let i = 0; i < tourists.length; i++) {
  tourists[i].visitedTbilisi = "Not Visited";

  for (let j = 0; j < tourists[i].cities.length; j++) {
    if (tourists[i].cities[j] === "Tbilisi") {
      tourists[i].visitedTbilisi = "Visited";
    }
  }
}

// TOTAL MONEY SPENT

for (let i = 0; i < tourists.length; i++) {
  tourists[i].expenses = {};
  tourists[i].expenses.totalSpent = 0;

  for (let j = 0; j < tourists[i].money.length; j++) {
    tourists[i].expenses.totalSpent += tourists[i].money[j];
  }
}

// AVERAGE MONEY SPENT

for (let i = 0; i < tourists.length; i++) {
  tourists[i].expenses.averageSpent = tourists[i].expenses.totalSpent / tourists[i].money.length;
}

// SPENT MOST MONEY

let maxSpent = 0;
let topSpender = 0;

for (let i = 0; i < tourists.length; i++) {
  tourists[i].expenses.spentMostMoney = false;
  if (tourists[i].expenses.totalSpent > maxSpent) {
    maxSpent = tourists[i].expenses.totalSpent;
    topSpender = i;
  }
}

tourists[topSpender].expenses.spentMostMoney = true;

//იგივე while ციკლის გამოყენებით:
// let i = 0;
// while(i < tourists.length) {
//     if(tourists[i].expenses.totalSpent > maxSpent) {
//         maxSpent = tourists[i].expenses.totalSpent;
//         topSpender = i;
//     }
//     i++;
// }

console.log(`Tourist who spent most money is ${tourists[topSpender].name} and they spent ${maxSpent}$`);
