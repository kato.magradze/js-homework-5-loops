let students = [];

students[0] = {
  name: "Jean",
  lastName: "Reno",
  age: 26,
  scores: {
    javascript: 62,
    react: 57,
    python: 88,
    java: 90,
  },
  individualGPA: {
    javascript: 1,
    react: 0.5,
    python: 3,
    java: 3,
  },
};

students[1] = {
  name: "Claude",
  lastName: "Monet",
  age: 19,
  scores: {
    javascript: 77,
    react: 52,
    python: 92,
    java: 67,
  },
  individualGPA: {
    javascript: 2,
    react: 0.5,
    python: 4,
    java: 1,
  },
};

students[2] = {
  name: "Van",
  lastName: "Gogh",
  age: 21,
  scores: {
    javascript: 51,
    react: 98,
    python: 65,
    java: 70,
  },
  individualGPA: {
    javascript: 0.5,
    react: 4,
    python: 1,
    java: 1,
  },
};

students[3] = {
  name: "Dam",
  lastName: "Square",
  age: 36,
  scores: {
    javascript: 82,
    react: 53,
    python: 80,
    java: 65,
  },
  individualGPA: {
    javascript: 3,
    react: 0.5,
    python: 2,
    java: 1,
  },
};

// TOTAL POINTS FOR EACH STUDENT

let subjects = ["javascript", "react", "python", "java"];

for (let i = 0; i < students.length; i++) {
  students[i].stats = {};
  students[i].stats.totalScore = 0;

  for (let j = 0; j < subjects.length; j++) {
    students[i].stats.totalScore += students[i].scores[subjects[j]];
  }

  console.log(`${students[i].name} ${students[i].lastName}'s Total Score: ${students[i].stats.totalScore}`);
}

console.log("\n");

// TOTAL AVERAGE FOR EACH STUDENT

for (let i = 0; i < students.length; i++) {
  students[i].stats.AvgScore = 0;

  for (let j = 0; j < subjects.length; j++) {
    students[i].stats.AvgScore +=
      students[i].scores[subjects[j]] / subjects.length;
  }

  console.log(`${students[i].name} ${students[i].lastName}'s Total Score: ${students[i].stats.AvgScore}`);
}

console.log("\n");

// GPA

const credits = {
  javascript: 4,
  react: 7,
  python: 6,
  java: 3,
};

credits.total = 0;

for (let i = 0; i < subjects.length; i++) {
  credits.total += credits[subjects[i]];
}

for (let i = 0; i < students.length; i++) {
  students[i].stats.GPA = 0;

  for (let j = 0; j < subjects.length; j++) {
    students[i].stats.GPA += students[i].individualGPA[subjects[j]] * credits[subjects[j]];
  }

  students[i].stats.GPA /= credits.total;

  console.log(`${students[i].name} ${students[i].lastName}'s GPA: ${students[i].stats.GPA}`);
}

console.log("\n");

// STUDENTS TOTAL AVERAGE

const studentTotals = {};
let sumOfAverages = 0;

for (let i = 0; i < students.length; i++) {
  sumOfAverages += students[i].stats.AvgScore;
}

studentTotals.totalAverage = sumOfAverages / students.length;
console.log(`Average Total Points for students: ${studentTotals.totalAverage}`);

console.log("\n");

// STUDENTS STATUS

for (let i = 0; i < students.length; i++) {
  students[i].stats.status =
    students[i].stats.AvgScore >= studentTotals.totalAverage
      ? "Red Diploma"
      : "Enemy of the People";
  console.log(`Status for ${students[i].name} ${students[i].lastName}: ${students[i].stats.status}`);
}
console.log("\n");

// HIGHEST GPA

let maxGPA = 0;
let bestByGPA = 0;

for (let i = 0; i < students.length; i++) {
  if (students[i].stats.GPA > maxGPA) {
    maxGPA = students[i].stats.GPA;
    bestByGPA = i;
  }
}

// იგივე while ციკლის გამოყენებით:
// let i = 0;
// while(i < students.length) {
//     if(students[i].stats.GPA > maxGPA) {
//         maxGPA = students[i].stats.GPA;
//         bestByGPA = i;
//     }
//     i++;
// }

console.log(`Student with highest GPA: ${students[bestByGPA].name} ${students[bestByGPA].lastName}`);
console.log("\n");

// BEST STUDENT WITH AVERAGE POINTS (21+)

let bestByAvgPoints21 = 0;
let maxValue = 0;

for (let i = 0; i < students.length; i++) {
  if (students[i].age >= 21 && students[i].stats.AvgScore > maxValue) {
    maxValue = students[i].stats.AvgScore;
    bestByAvgPoints21 = i;
  }
}

//იგივე while ციკლის გამოყენებით:
// let i = 0;
// while((students[i].age >= 21) && (students[i].stats.AvgScore > maxValue)) {
//     maxValue = students[i].stats.AvgScore;
//     bestByAvgPoints21 = i;
//     i++;
// }

console.log(`Student with highest average points (21+): ${students[bestByAvgPoints21].name} ${students[bestByAvgPoints21].lastName}`);
console.log("\n");

//BEST STUDENT IN FRONT-END SUBJECTS WITH AVEVRAGE POINTS 

let frontSubjects = [];

for (let i = 0; i < subjects.length; i++) {
  if (subjects[i] === "javascript" || subjects[i] === "react") {
    frontSubjects[frontSubjects.length] = subjects[i];
  }
}

for (let i = 0; i < students.length; i++) {
  students[i].stats.frontAverage = 0;

  for (let j = 0; j < frontSubjects.length; j++) {
    students[i].stats.frontAverage += students[i].scores[frontSubjects[j]];
  }

  students[i].stats.frontAverage /= frontSubjects.length;
}

let maxFront = 0;
let bestInFront = 0;

for (let i = 0; i < students.length; i++) {
  if (students[i].stats.frontAverage > maxFront) {
    maxFront = students[i].stats.frontAverage;
    bestInFront = i;
  }
}

console.log(`Student with highest average points in Front-end subjects: ${students[bestInFront].name} ${students[bestInFront].lastName}`);
